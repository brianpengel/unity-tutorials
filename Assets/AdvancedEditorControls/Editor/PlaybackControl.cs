using UnityEditor;
using UnityEngine;

namespace AdvancedEditorControls {

    public static partial class UIBehaviour {

        internal static readonly int BEHAVIOUR_TIME_CONTROL_HASH = "BEHAVIOUR_TIME_CONTROL_HASH".GetHashCode();
        internal const string BEHAVIOUR_TIME_CONTROL_TOGGLE_COMMAND = "TimeControlToggle";

        private static class Style {
            public static GUIContent PauseIcon = EditorGUIUtility.IconContent("PauseButton");
            public static GUIContent PlayIcon = EditorGUIUtility.IconContent("PlayButton");

            public static GUIStyle PlayButton = "toolbarbutton";
            public static GUIStyle Toolbar = "toolbar";
        }

        public static float PlaybackControl(Rect position, float val, float maxVal, float playbackSpeed) {
            bool isPlaying;
            val = PlaybackControlBehaviour(position, val, maxVal, playbackSpeed, out isPlaying);

            if(Event.current.type == EventType.Repaint)
                Style.Toolbar.Draw(position, false, isPlaying, isPlaying, false);

            float minW, maxW;
            Style.PlayButton.CalcMinMaxWidth(Style.PlayIcon, out minW, out maxW);

            Rect buttonRect = new Rect(position);
            buttonRect.width = minW;

            GUIContent iconToUse = isPlaying ? Style.PauseIcon : Style.PlayIcon;

            if(GUI.Button(buttonRect, iconToUse, Style.PlayButton)) {
                Event e = EditorGUIUtility.CommandEvent(BEHAVIOUR_TIME_CONTROL_TOGGLE_COMMAND);
                EditorWindow.focusedWindow.SendEvent(e);
            }

            return val;
        }


        public static float PlaybackControlBehaviour(Rect position, float val, float maxVal, float playbackSpeed, out bool isPlaying) {
            int controlID = EditorGUIUtility.GetControlID(BEHAVIOUR_TIME_CONTROL_HASH, FocusType.Keyboard, position);
            PlaybackControlState state = (PlaybackControlState) EditorGUIUtility.GetStateObject(typeof(PlaybackControlState), controlID);
            Event e = Event.current;

            switch(e.GetTypeForControl(controlID)) {
                case EventType.Repaint:
                    if(state.IsPlaying) {
                        val = val + (state.GetDeltaTime() * playbackSpeed);

                        if(val > maxVal)
                            val -= maxVal; 
                        GUI.changed = true;
                    }
                    break;

                case EventType.ValidateCommand:
                    if(e.commandName == BEHAVIOUR_TIME_CONTROL_TOGGLE_COMMAND)
                        e.Use();
                    break;

                case EventType.ExecuteCommand:
                    if(e.commandName == BEHAVIOUR_TIME_CONTROL_TOGGLE_COMMAND)
                        state.SetIsPlaying(!state.IsPlaying);
                    break;

                case EventType.MouseDown:
                    if (position.Contains(e.mousePosition)) {
                        EditorGUIUtility.keyboardControl = controlID;
                        Debug.Log("Focus me");
                    }
                    break;

                case EventType.KeyDown:
                    if (EditorGUIUtility.keyboardControl == controlID) {
                        Debug.Log("We've got focus");
                        if (e.keyCode == KeyCode.LeftArrow) {
                            val = Mathf.Clamp(val - .1f, 0, maxVal);
                            state.SetIsPlaying(false);
                            GUI.changed = true;
                            e.Use();
                        }

                        else if (e.keyCode == KeyCode.RightArrow) {
                            val = Mathf.Clamp(val + .1f, 0, maxVal);
                            state.SetIsPlaying(false);
                            GUI.changed = true;
                            e.Use();
                        }

                        else if (e.keyCode == KeyCode.Space) {
                            state.SetIsPlaying(!state.IsPlaying);
                            e.Use();
                        }
                    }
                    break;
            }

            isPlaying = state.IsPlaying;
            return val;
        }
        
        internal class PlaybackControlState {
            private double lastUpdatedTime;
            private bool isPlaying;

            public bool IsPlaying => isPlaying;

            public float GetDeltaTime() {
                double timeSinceStartup = EditorApplication.timeSinceStartup;
                double deltaTime = timeSinceStartup - this.lastUpdatedTime;
                this.lastUpdatedTime = timeSinceStartup;
                return (float) deltaTime;
            }

            public void SetIsPlaying(bool b) {
                if(b) this.lastUpdatedTime = EditorApplication.timeSinceStartup;
                this.isPlaying = b;
            }
        }
    }
}

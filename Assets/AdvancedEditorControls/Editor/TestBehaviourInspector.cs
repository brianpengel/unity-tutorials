﻿using UnityEngine;
using UnityEditor;

namespace AdvancedEditorControls {

    [CustomEditor(typeof(AdvancedEditorControls.TestBehaviour))]
    public class TestBehaviourInspector : Editor {

        private SerializedProperty maxPlaybackProperty;
        private SerializedProperty playbackProperty;
        
        private void OnEnable() {
            this.maxPlaybackProperty = serializedObject.FindProperty("maxPlaybackValue");
            this.playbackProperty = serializedObject.FindProperty("playbackValue");
        }

        public override void OnInspectorGUI() {
            serializedObject.Update();

            var rect = EditorGUILayout.GetControlRect(false, EditorGUIUtility.singleLineHeight);
            this.playbackProperty.floatValue = UIBehaviour.PlaybackControl(rect, this.playbackProperty.floatValue, this.maxPlaybackProperty.floatValue, 1f);

            serializedObject.ApplyModifiedProperties();
            base.OnInspectorGUI();
        }
    }
}

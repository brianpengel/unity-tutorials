﻿using UnityEngine;

namespace AdvancedEditorControls {

    [AddComponentMenu("AEC/TestBehaviour")]
    public class TestBehaviour : MonoBehaviour {
        
        [SerializeField] 
        private float maxPlaybackValue = 1;

        [SerializeField] 
        private float playbackValue;
    }
}
